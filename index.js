const fs = require('fs') ;
const fetch = require('node-fetch');
const {parse}  = require('node-html-parser');

const getFolder = (index) => {
  var result =  Math.ceil(index/500)*500+ '';
  if (!fs.existsSync(result)) {
    fs.mkdirSync(result);
  }
  return result;
}

const downloadMp3File = (async (index, url,title) => {
  const res = await fetch(url);
  const fileStream = fs.createWriteStream(getFolder(index)+'/'+(index+'').padStart(4,'0')+'-'+title+'.mp3');
  await new Promise((resolve, reject) => {
      res.body.pipe(fileStream);
      res.body.on("error", reject);
      fileStream.on("finish", resolve);
  });

});

const writeContent = (index , title,content,onlineDate) =>{
  fs.writeFile(getFolder(index)+'/'+(index+'').padStart(4,'0')+'-'+title+'.txt', title+' ('+onlineDate+')\n'+content, function (err) {
    if (err) return console.log(err);
    console.log(index+' done');
  });
}

(async function main() {
  for (var i=1;i<4990 ;i++) {
    const url = 'https://app7.rthk.hk/elearning/1minreading/item.php?aid='+i;
    try {
      const res = await fetch(url).then((response) => {
        return response.text();
      });
      const htmlDom = parse(res);
      const mp3Url = 'https:\/\/app7.rthk.hk\/mp3\/elearning\/1minreading/ly/ch'+ (i+'').padStart(2,'0')  +'.mp3';
      if (htmlDom.querySelector('body > script:nth-child(4)').innerText.includes('1minreading\/ly\/'))  {
        console.log(i+' ly : download');
        const filteredResult = 
          htmlDom.querySelectorAll('head > meta')
          .filter( (e) => e.getAttribute('name') === 'onlineDate')
          .reduce(
            (result, e) => {
              result[(e.getAttribute('property')) ? e.getAttribute('property'):e.getAttribute('name') ] = e.getAttribute('content');
              return result;
            },{})
          ;
        
        const title = htmlDom.querySelector('body > div > div.item-info > div.item-title').innerText;
        const content =  htmlDom.querySelector('body > div > div.item-info > div.item-desc')
                                  .innerHTML
                                    .replace(/[\r\n]/gm, '')
                                    .replace(/<br><br>/g,'\n')
                                    .replace(/<br>/g,'\n');
        const onlineDate = filteredResult['onlineDate'];

        await downloadMp3File(i, mp3Url , title);
        writeContent(i , title , content , onlineDate);
      }else {
        console.log(i+' non ly : skip');
      }
    } catch (err) {
      console.error(err);
    }
  }  
})();
